<?php
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saveprogram extends CI_Controller {
          
    public function __Construct() {
        
        parent::__Construct();
        $this->load->model('Api/Saveprogram_model');
		//echo '<pre>'; print_r($_POST); exit;    
        $this->load->library('JWT');
       
    }

      public function SaveUser(){ 
            
            $program_id = $this->input->post('program_id');
			$machine_id = $this->input->post('machine_id');
            $user_id = $this->input->post('user_id');			
            $status = $this->input->post('status');
            $repetation = $this->input->post('repetation');
            $pin = $this->input->post('pin');
            $time = $this->input->post('time');
            $series = $this->input->post('series');
            $heart_rate = $this->input->post('heart_rate');
			$type = $this->input->post('type');
			$user_week = $this->Saveprogram_model->get_week($user_id);
			$week = $user_week->week;
			$tr_date  = date('Y-m-d');
			
             $data = array(
            'program_id' => $program_id,
			'machine_id' => $machine_id,
            'user_id' => $user_id,
            'status' => $status,
            'repetation' => $repetation,
            'pin' => $pin,
            'time' => $time,
            'series' => $series,
            'heart_rate' => $heart_rate,
			'week' => $week,
			'type' => $type,
			'training_date' => $tr_date,
            );
             
             if($status == 'complete'){

              $check_training_week = $this->Saveprogram_model->check_training($user_id,$tr_date); 
			// if(!empty($check_training_week)){
				 
							$record = $this->Saveprogram_model->check_record($program_id,$user_id,$machine_id,$tr_date);
							
							if(!empty($record)){
								$id = $record->id;
								
										$update = array(
									'program_id' => $program_id,
									'machine_id' => $machine_id,
									'status' => $status,
									'repetation' => $repetation,
									'pin' => $pin,
									'time' => $time,
									'series' => $series,
									'heart_rate' => $heart_rate,
									'week' => $week,
									'type' => $type,
									'training_date' => $tr_date,
									);
										$this->db->where('user_id',$user_id);
										$this->db->where('id',$id);
										$this->db->update('t_save_strength_program',$update);
										if ($this->db->trans_status() === TRUE)
											{
												$getdata = $id;
											}
										
							}
							else
							{
							$getdata = $this->Saveprogram_model->SaveStatus($data);
							}
			// }
            if(!empty($getdata)){
            echo json_encode(array('jwt'=>$this->jwt->encode(array(
            'response'=>$getdata,
   			'message'=>'program  saved',
            'status'=>1
            ), CONSUMER_SECRET)));
            }else{
	         echo json_encode(array('jwt'=>$this->jwt->encode(array(
	         'response'=>'',
	         'message'=>'program not saved, Your week is not started yet',
	         'status'=>0
	          ), CONSUMER_SECRET)));
	                            
	       }
            
            }else{

            	echo "program not saved";
            }
        }
        public function get_machine_details(){

        	$result = $this->Saveprogram_model->machineDetails();
        		//echo '<pre>';print_r($result);
        		 if(!empty($result)){
		            echo json_encode(array('jwt'=>$this->jwt->encode(array(
		            'response'=>$result,
		   			'status'=>1
		            ), CONSUMER_SECRET)));
		            }else{
			         echo json_encode(array('jwt'=>$this->jwt->encode(array(
			         'response'=>'',
			         'status'=>0
			          ), CONSUMER_SECRET)));
			                            
			       }
        }
   }  


?>
