<?php 

//error_reporting(-1);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Login extends CI_Controller {
	 public function __construct()
		{
		    parent::__construct();
			$this->load->model('Api/Login_model');
			$this->load->library('JWT');
		}
		
		Public function login_user(){
			
			$email = $this->input->post('email');
			$pass = $this->input->post('password');
			$data = $this->Login_model->login($email,$pass);
			
			if(!empty($data)){
			
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
         'response'=>$data,
         'message'=>'successfully login.',
         'status'=>1
          ), CONSUMER_SECRET)));
			}
			else{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
         'response'=>array(),
         'message'=>'Wrong Email and Password',
         'status'=>0
          ), CONSUMER_SECRET)));
			//	echo json_encode(array('response'=>'','status'=>0,'message'=>'last name and coach id is not valid.' ));
				
			}
			
		}
		
		public function getUserList(){
		

			$device_code = $this->input->post('ids');
			$device_name =  str_replace(",","','",$device_code);
		 
			$result = $this->Login_model->get_device_id($device_name,$device_code);
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
				 'response'=>$result,
				 'status'=>1
				  ), CONSUMER_SECRET)));
		}
		
		Public function registerHRmonitor(){
			$client_id = $this->input->post('client_id');
			$data['device_code'] = $hr_monitor_id = $this->input->post('hr_monitor_id');
			$data['r_device_type_id'] = 1;
			if($this->input->post('last_name')){
				//echo "hello";
				//die;
				$last_name = $this->input->post('last_name');
				$data = $this->Login_model->check_lastname_user($last_name);
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
											 'response'=>$data,
											 'status'=>12
											  ), CONSUMER_SECRET)));
			}
			else if($client_id){
			
			
			$is_existuser = $this->Login_model->check_valid_user($client_id,$last_name);
			
			if($is_existuser>0){
			$record = $this->Login_model->check_id($hr_monitor_id);
			if($record['count']<=0){
				$insert_id = $this->Login_model->insert_device($data,'t_device');
				
			
			}
			else{
				$insert_id = $record['device_id'];  
			}
			$detail = $this->Login_model->check_client($insert_id,$client_id);
			if($detail<=0){
				
				$data = array('r_user_id'=>$client_id,
							  'r_device_id'=>$insert_id,
							  'time'=>date('h:i:s')
							  );
							 // print_r($data);
							  //die;
				$insert_id = $this->Login_model->insert_device($data,'t_device_member');
				if($insert_id!='')
				{
					$data = $this->Login_model->device_user($client_id);
					echo json_encode(array('jwt'=>$this->jwt->encode(array(
											 'response'=>$data,
											 'status'=>11
											  ), CONSUMER_SECRET)));
				}
				else{
						echo json_encode(array('jwt'=>$this->jwt->encode(array(
											 'response'=>'data not inserted',
											 'status'=>10
											  ), CONSUMER_SECRET)));
				}
			}
			else{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
											 'response'=>'Already exist',
											 'status'=>00
											  ), CONSUMER_SECRET)));
			}
			}
			else{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
											 'response'=>'Invalid user',
											 'status'=>01
											  ), CONSUMER_SECRET)));
			}
			
		}  
		}
		// get machine info by vijay
		public function get_machine_info()
		{
			$user_id = $this->input->post('user_id');
			$machine_id = $this->input->post('machine_id');
			$result  = $this->Login_model->get_machine_info($user_id);
			//print_r($result);
			$p_id ="";
			if(!empty($result))
			{
				if($result['r_strength_program_free_id'] != "0")
				{
					$p_id = $result['r_strength_program_free_id'];
				} else if($result['r_strength_program_mix_id'] != "0")
				{
					$p_id = $result['r_strength_program_mix_id'];
				} else if($result['r_strength_program_circuit_id'] != "0")
				{
					$p_id = $result['r_strength_program_circuit_id'];
				}
			}
			
			$result2 = $this->Login_model->get_other_machine_info($p_id,$user_id,$machine_id,$result['program_type']);
			echo json_encode(array('jwt'=>$this->jwt->encode($result2, CONSUMER_SECRET)));
											  
		//print_r($result2);
		//	die;
		}

 }
 ?>