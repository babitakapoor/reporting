<?php 
error_reporting(1);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Voucherlist extends CI_Controller {
	 public function __construct()
		{
		    parent::__construct();
			$this->load->model('voucher_model');
		}

//***********************************************************************//
//*******************function used for voucher list**********************//
//***********************************************************************//
	      public function vlist()
			 { 
			 	$data = $this->voucher_model->voucherlist();
			 	if(!empty($data))
			 	{
			 	echo json_encode(array('status'=>1,'data'=>$data));
				}
				else
				{
				echo json_encode(array('status'=>0,'data'=>'No list found!'));	
				}
			 }
			 
//***********************************************************************//
//*******************function used for coach list**********************//
//***********************************************************************//
			public function coachList()
			 { 
				$user_id = $this->input->post('user_id');
				$club_id = $this->input->post('club_id');
				$user_typeId = $this->input->post('user_typeId');
				if($user_typeId == 9)
				{
				$data = $this->voucher_model->coachListAll($user_id,$club_id,$user_typeId);
				}
				else if($user_typeId == 1)
				{
				$data = $this->voucher_model->coachListOne($user_id,$club_id,$user_typeId);
				}
			 	if(!empty($data))
			 	{
			 	echo json_encode(array('status'=>1,'data'=>$data));
				}
				else
				{
				echo json_encode(array('status'=>0,'data'=>'No list found!'));	
				}
			 }

			 public function strength_progWeek(){

			    $id = $this->input->post('r_strength_pgmid');
			    $r_week = $this->input->post('r_week');
			   
			    $result = $this->voucher_model->program_week($id);
			    if(!empty($result)){
			          echo json_encode(array('response'=>$result ,'status'=>1));  
			        }else{
			          
			          echo json_encode(array('response'=>'' ,'status'=>0)); 
			          
			        }

			  }

			  public function save_strengthProg(){

			  	$user_id = $this->input->post('user_id');
			  	$trainingsweek = $this->input->post('trainingsweek');
			  	$series = $this->input->post('series');
			  	$herh = $this->input->post('herh');
			  	$tijd = $this->input->post('tijd');
			  	$kracht = $this->input->post('kracht');
			  	$rust = $this->input->post('rust');

			  				$data = array(
				
						'user_id' => $user_id, 	
						'trainingsweek' => $trainingsweek, 	
						'series' => $series, 	
						'herh' => $herh, 	
						'tijd' => $tijd, 	
						'kracht' => $kracht,
						'rust' => $rust 	
							);

			  	$result = $this->voucher_model->insert_data_strength($data);	
		
				$msg = array('message'=>'data Succesfully inserted.' ,'status'=>'1');
				
					echo json_encode($msg);

			  }
				
 }
 ?>