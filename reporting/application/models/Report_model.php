<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {
/*********************************
fetch data according to consult type
***********************************/
public function get_fitness($user_id)
	{
		$this->db->select('fitness_level');
		$this->db->from('t_user_test_parameter');
		$this->db->where(array('r_user_id'=>$user_id,'status'=>0));
		$this->db->order_by('user_test_parameter_id','DESC');  
		$result = $this->db->get();   
		$result = $result->result();
		
		if(!empty($result))
		{
		return $result[0]->fitness_level;  
		}
		else
		{
			return 0;
		}
		
	}
	public function get_flexibility($user_id)
	{
		$this->db->select('flex_level');  
		$this->db->from('t_flexibility_test');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('flexibility_test_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();
		
		if(!empty($result))
		{
			if($result[0]->flex_level != '')
			{
				return $result[0]->flex_level;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
		  
		
	}
	public function get_strength($user_id)
	{
		$this->db->select('strength_level');  
		$this->db->from('t_user_strength_level');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('t_user_strength_level_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();
		
		if(!empty($result))
		{
			if($result[0]->strength_level != '')
			{
				return $result[0]->strength_level;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
		  
		
	}
	
	/*********************************
fetch data for body_composition
***********************************/
public function get_body_composition($user_id,$gender,$age)
{
 $bmi=0;
 $waist=0;
 $fat_percentage=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  //print_r($result);
  //die;
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
  
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='BODY_MASS_INDEX')
   {
    
    $bmi=$this->get_bmi_level($test_value);
    
   }   
   else if($test_item!='' && $test_item=='FAT_PERCENTAGE')
   {
    $fat_level=$this->get_fat_level($test_value,$age,$gender);
	$fat_percentage = $test_value;
    
   }
   
   else if($test_item!='' && $test_item=='BELLY_GRITH')
   {
    $waist=$this->get_waist_level($test_value,$gender);
   }
   else
   {
    continue;
   }
   
     
  }
  
  
  $body_composition = ($bmi + ($waist*2) + ($fat_level*2))/5;
     $data=$body_composition;
   
  return $data;
 }
 else
 {
 return false;
 } 
  
 
}

/**************for getting bmi level*************/
 public function  get_bmi_level($test_value)
 {
 
  $this->db->select('BMI_level');
  $this->db->from('t_bmi_level');
  $this->db->where('bmi_min <=',$test_value);
  $this->db->where('bmi_max >',$test_value);
  //$this->db->where("$test_value BETWEEN bmi_min AND bmi_max");
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $bmi)
   {
    
    return $bmi->BMI_level;
   }
  }
  else
  {
   return false;
  }
 }
 /**************for fat level************/
 public function  get_fat_level($test_value,$age,$gender)
 {
 
  $this->db->select('fatlevel');
  $this->db->from('t_fat_level');
  $this->db->where('age_min <=',$age);
  $this->db->where('age_max >',$age);
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >',$test_value);
  $this->db->where('gender',$gender);
  //$this->db->where("$test_value BETWEEN bmi_min AND bmi_max");
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $fat)
   {
    
    return $fat->fatlevel;
   }
  }
  else
  {
   return false;
  }
 }
 /************for getting waist level********/
 public function  get_waist_level($test_value,$gender)
 {
  
  $this->db->select('level');
  $this->db->from('t_waist_level');
  $this->db->where('waist_min <=',$test_value);
  $this->db->where('waist_max >',$test_value);
  //$this->db->where("$test_value BETWEEN waist_min AND waist_max");
  $this->db->where('gender',$gender);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $waist)
   {
    
    return $waist->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 /*********get user detail***********/
 public function  get_user($user_id)
 {
  
  $this->db->select('*');
  $this->db->from('t_users');
  $this->db->where('user_id',$user_id);
  $query=$this->db->get();
  $result=$query->row();
  if(!empty($result)){
   return $result;
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting cholestrol***************/
 public function get_cholestrol($user_id)
{
 $cholestrol=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='CHOLESTROL')
   {
    
    $cholestrol=$this->get_chol_level($test_value);
   }
   else
   {
    continue;
   }
   
  
   
  }
 return $cholestrol;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting cholestrol level********/
 public function  get_chol_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_cholestrol_level');
  $this->db->where('c_min <=',$test_value);
  $this->db->where('c_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $chol)
   {
   
    return $chol->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting Visceral_fat %***************/
 public function get_visceral_fat($user_id)
{
 
 $visceral=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='VISCERAL_FAT_PERCENTAGE')
   {
    
    $visceral=$this->get_vis_level($test_value);
   }
   else
   {
    continue;
   }
     
  }
 return $visceral;
 }
 else
 {
 return false;
 } 
  
 
}
/************for getting Visceral fat level********/
 public function  get_vis_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_visceral_fat_level');
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $vis)
   {
    
    return $vis->level;
   }
  }
  else
  {
   return false;
  }
 }
 /***************for getting Glucose***************/
 public function get_glucose($user_id)
{
 
 $glucose=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='GLUCOSE_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,1);
   }
   if($test_item!='' && $test_item=='GLUCOSE_NOT_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,0);
   }
   else
   {
    continue;
   }
     
  }
  return $glucose;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting glucose level********/
 public function  get_glucose_level($test_value,$sober)
 {
 
  $this->db->select('level');
  $this->db->from('t_glucose_level');
  $this->db->where('glucose_min <=',$test_value);
  $this->db->where('glucose_max >',$test_value);
  $this->db->where('sober',$sober);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $gluc)
   {
    
    return $gluc->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 
 /*****model function for getting vita_16 value************/
 public function get_vita_sixteeen($user_id,$iwant=null)
 { 
	 $energy=0;
 $mot=0;
 $regilience=0;
 $data=0;
  
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $this->db->order_by('t_testing_measuring_transaction_id','ASC');

 $query=$this->db->get();
 $result=$query->result();

 if($result>0){
 
  foreach($result as $results)
  {
    

   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
 
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='ENERGY')
   {
    $energy_val= $test_value;


    $energy=$this->get_vita_level($test_value,'ENERGY');
    
   }   
   else if($test_item!='' && $test_item=='MOTIVATION')
   {
    $mot_val= $test_value;
     $mot=$this->get_vita_level($test_value,'MOTIVATION');
	 
   }
   
   else if($test_item!='' && $test_item=='RESILIENCE')
   { 
    $regilience_val= $test_value;
    $regilience=$this->get_vita_level($test_value,'RESILIENCE');
   }
   else
   {
    continue;
   }
     
  }


   $vita= ($energy+$mot+$regilience)/3;
  $data = array('vita_16'=>$vita,
              'energy'=>$energy_val,
              'motivation'=>$mot_val,
              'resilience'=>$regilience_val
              );

    
	  //  return array('energy'=>$energy_val,'motivation'=>$mot_val,'resilience'=>$regilience_val);


  return $data;
 }
 else
 {
 return false;
 } 
  
 
 }
 /************for getting e level********/
 public function  get_vita_level($test_value,$type)
 {
 if($type =='ENERGY')
 {
  $this->db->select('level');
  $this->db->from('t_vita_energy_level');
  $this->db->where('energy_min <',$test_value);
  $this->db->where('energy_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $energy)
   {
    
    return $energy->level;
   }
  }
  else
  {
   return false;  
  }
 }
 else if($type =='MOTIVATION'){
	 $this->db->select('motivation_level');
  $this->db->from('t_vita_motivation_level');
  $this->db->where('mot_min <=',$test_value);
  $this->db->where('mot_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $mot)
   {
    
    return $mot->motivation_level; 
   }
  }
  else
  {
   return false;  
  }
	 
 }
 else if($type =='RESILIENCE')
 {
	 $this->db->select('level');
  $this->db->from('t_vita_resilience_level');
  $this->db->where('res_min <=',$test_value);
  $this->db->where('res_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $res)
   {
    
    return $res->level;
   }
  }
  else
  {
   return false;  
  }
 }
 }
 
 
 /*****model function for getting stress Sleep mindfullnes value************/
 public function get_stress_sleep_mindfullness_level($user_id)
 {
	$data['stress'] = 0;
	$data['sleep'] =0;
 $data['mindful'] =0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $this->db->where('t_testing_measuring_transaction.r_test_id',9);
 //$this->db->order_by('t_testing_measuring_transaction_id','DESC');
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
   
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
 
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='STRESS')
   {
    $data['stress_val']=$test_value;
    $data['stress']=$this->get_stress_sleep_mindfull_level($test_value,'STRESS');
    
   } 
  
   else if($test_item!='' && $test_item=='SLEEP')
   {
     $data['sleep_val']=$test_value;
    $data['sleep']=$this->get_stress_sleep_mindfull_level($test_value,'SLEEP');
    
   }
   else if($test_item!='' && $test_item=='MINDFULNESS')
   {
     $data['mindful_val']=$test_value;
    $data['mindful']=$this->get_stress_sleep_mindfull_level($test_value,'MINDFULNESS');
    
   }
   {
    continue;
   }
   
     
  }
 
  
  return $data;
 }
 else
 {
 return false;
 } 
  
 
 }
/*********************************
Personal Goal evaluations
**********************************/ 
 public function personal_goal($user_id)
 {
	 
	  $userperonalgoal =$this->db->select('target')->from('t_user_test_parameter')->where(array('r_user_id'=>$user_id))->order_by("user_test_parameter_id", "desc")->get()->row();
	
	  if(count($userperonalgoal)>0)
	  {
		  return $userperonalgoal->target;
		  
	  }
	  else
	  {
		  return 0;  
		  
	  }
 
 }
 
 /************for getting e level********/
 public function  get_stress_sleep_mindfull_level($test_value,$type)
 {
 if($type =='MINDFULNESS')
 {
  $this->db->select('level');
  $this->db->from('t_mind_fullness_level');
  $this->db->where('mind_min <=',$test_value);
  $this->db->where('mind_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $mind)
   {
    
    return $mind->level;
   }
  }
  else
  {
   return false;  
  }
 }
 else if($type =='SLEEP'){
	 $this->db->select('level');
  $this->db->from('t_sleep_level');
  $this->db->where('SLEEP_min <=',$test_value);
  $this->db->where('SLEEP_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $SLEEP)
   {
    
    return $SLEEP->level; 
   }
  }
  else
  {
   return false;  
  }
	 
 }
 else if($type =='STRESS')
 {
	 $this->db->select('level');
  $this->db->from('t_stress_level');
  $this->db->where('stress_min <=',$test_value);
  $this->db->where('stress_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $stress)  
   {
    
    return $stress->level;
   }
  }
  else
  {
   return false;  
  }
 }
 }
 Public function get_level_description($type_id,$item_id,$level)
 {
   $this->db->select('*');
   $this->db->from('t_level_description');
   $this->db->where('type_id',$type_id);
   $this->db->where('item_id',$item_id);
   $this->db->where('level',$level);
   $query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result->level_description;
   }
   else{
     return false;
    }
   
 }
//for admin support link//
public function get_link()
{
  $this->db->select('link');
  $this->db->from('t_support_link');
$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result->link;
   }
   else{
     return false;
    }
} 

public function get_personal_info($user_id)
{
  $this->db->select('*');
  $this->db->from('t_personalinfo');
  $this->db->where('r_user_id',$user_id);
	$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result;
   }
   else{
     return false;
    }
} 
/**********for coach id***********/
public function coach_id($user_id){
	
	$this->db->select('*');
	$this->db->from('t_coach_member');
	$this->db->where('r_user_id',$user_id);
	$query = $this->db->get();
   $result=$query->row();
   
     if(!empty($result))
	   {
		return $result->r_coach_id;
	   }
   else
	{
     return false;
    }
	
}
public function getinfo($coach_user_id){
	 $this->db->select('*');
  $this->db->from('t_users');
  $this->db->join('t_personalinfo','t_users.user_id=t_personalinfo.r_user_id','inner');
  $this->db->where('user_id',$coach_user_id);
	$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result;
   }
   else{
     return false;
    }
	
}
public function stepcheckgetinfo($user_id,$step_id){
	$data['step_entry_type'] = $step_id;
	$this->db->where('r_user_id', $user_id);
    $this->db->update(t_personalinfo, $data);
	$afftectedRows = $this->db->affected_rows();
	return $afftectedRows;
}
}

?>